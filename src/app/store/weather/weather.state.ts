import { Action, NgxsOnInit, Selector, State, StateContext } from '@ngxs/store';
import { createEntityAdapter, Dictionary, EntityAdapter, EntityState } from '@ngrx/entity';
import { of } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ReceiveCityWeatherModel, SaveCity, SelectCity } from './weather.action';
import { WheatherService } from '../../api';
import { LocalStorageKeys } from '../../common';

import { CityWeather } from '../../models';

interface StateModel extends EntityState<CityWeather> {
  selectedCityList?: string[];
  currentCity?: string;
}

const adapter: EntityAdapter<CityWeather> = createEntityAdapter<CityWeather>({
  selectId: (entity: CityWeather) => entity.name,
});

const initialState: StateModel = adapter.getInitialState({
  selectedCityList: [],
  currentCity: undefined,
});

@State<StateModel>({
 defaults: initialState,
 name: 'CITY_WEATHER'
})
export class CityWeatherState implements NgxsOnInit {

  constructor(private readonly wheatherService: WheatherService) {
  }

  @Selector()
  public static getEntities(state: StateModel): Dictionary<CityWeather> {
    return state.entities;
  }

  @Selector()
  public static getSelectedCityList(state: StateModel): string[] {
    return state.selectedCityList;
  }

  @Selector()
  public static getCurrentCity(state: StateModel): string {
    return state.currentCity;
  }

  public ngxsOnInit(ctx: StateContext<StateModel>): void {
    const currentCity = localStorage.getItem(LocalStorageKeys.CURRENT_CITY);
    const selectedCityList = localStorage.getItem(LocalStorageKeys.SELECTED_CITIES);
    const newState: Partial<StateModel> = {};
    let isLocalStorageValue = false;

    if (currentCity) {
      newState.currentCity = currentCity;
      isLocalStorageValue = true;
    }

    if (selectedCityList) {
      newState.selectedCityList = selectedCityList.split(',');
      isLocalStorageValue = true;
    }
    if (isLocalStorageValue) {ctx.patchState(newState); }
  }

  @Action(ReceiveCityWeatherModel)
  public receiveCityWeatherModel(ctx: StateContext<StateModel>, action: ReceiveCityWeatherModel) {
    const state = ctx.getState();

    return this.wheatherService.getCityWeather(action.payload || state.currentCity)
      .pipe(
        tap((cityWeatherList: CityWeather) => {
          ctx.patchState(
            adapter.upsertOne(cityWeatherList, state),
          );
        })
      );
  }

  @Action(SaveCity)
  public saveCity(ctx: StateContext<StateModel>, action: SaveCity) {
    const state = ctx.getState();

    if (state.entities[action.payload]) {
      ctx.patchState({
        currentCity: action.payload,
      })
      localStorage.setItem(LocalStorageKeys.CURRENT_CITY, action.payload);

      return of(undefined);
    } else {

      return this.wheatherService.getCityWeather(action.payload)
      .pipe(
        tap((cityWeatherList: CityWeather) => {
          const newCityList = this.getUniqueValueArray(state.selectedCityList, cityWeatherList.name);
          ctx.patchState({
            ...adapter.upsertOne(cityWeatherList, state),
            selectedCityList: newCityList,
            currentCity: cityWeatherList.name
          })
          localStorage.setItem(LocalStorageKeys.CURRENT_CITY, cityWeatherList.name);
          localStorage.setItem(LocalStorageKeys.SELECTED_CITIES, newCityList.join(','));
        })
      );
    }
  }

  @Action(SelectCity)
  public selectCity(ctx: StateContext<StateModel>, action: SelectCity) {
    ctx.patchState({currentCity: action.payload});
  }

  private getUniqueValueArray(itemList: string[], newItem: string): string[] {
    return itemList.some((item: string) => item === newItem)
      ? itemList
      : [...itemList, newItem];
  }
}
