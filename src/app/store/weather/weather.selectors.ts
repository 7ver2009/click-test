import { createSelector, Selector } from '@ngxs/store';
import { createEntityAdapter, Dictionary, EntityAdapter, EntityState, Update } from '@ngrx/entity';

import { CityWeatherState } from './weather.state';
import { CityWeather } from '../../models';

export class CityWeatherSelectors {

  @Selector([CityWeatherState.getEntities, CityWeatherState.getCurrentCity])
  public static getWeatherForCurrentCity(entities: Dictionary<CityWeather>, cityId: string) {
    return entities[cityId];
  }
}
