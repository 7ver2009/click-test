export enum ActionTypes {
  ReceiveCityWeatherModel = '[Weather] Receive City Weather model',
  SaveCity = '[Weather] Save City',
  SelectCity = '[Weather] Select City',
}

export class ReceiveCityWeatherModel {
  public static readonly type = ActionTypes.ReceiveCityWeatherModel;

  constructor(public readonly payload?: string) { }
}

export class SaveCity {
  public static readonly type = ActionTypes.SaveCity;

  constructor(public readonly payload: string) { }
}

export class SelectCity {
  public static readonly type = ActionTypes.SelectCity;

  constructor(public readonly payload: string) { }
}
