import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';

import { CityWeatherState } from './weather.state';

@NgModule({
  imports: [
    NgxsModule.forFeature([CityWeatherState]),
  ]
})
export class CityWeatherStateModule { }