import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@env/environment';

export abstract class BaseService {

  protected readonly abstract serviceUrl: string;

  constructor(private readonly http: HttpClient) {
  }

  protected get<T>(pathFragment: string, requestParams: {[key: string]: string}): Observable<T> {
    const params = this.getRequestParams(requestParams);
    const fullUrl = this.getFullUrl(pathFragment);

    return this.http.get(fullUrl, {params}) as Observable<T>;
  }

  private getFullUrl(pathFragment: string): string {
    const lastPathfragment = pathFragment ? `/${pathFragment}` : '';

    return `${environment.api}/${this.serviceUrl}${lastPathfragment}`;
  }

  private getRequestParams(params: {[key: string]: string}): {[key: string]: string} {
    return {...params, appid: environment.token};
  }
}
