import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BaseService } from './base.service';
import { CityWeather } from '../models';

@Injectable({
  providedIn: 'root'
})
export class WheatherService extends BaseService {

  protected readonly serviceUrl = 'data/2.5/weather';

  constructor(http: HttpClient) {
    super(http);
  }

  getCityWeather(city: string): Observable<CityWeather> {
    return this.get<CityWeather>('', {q: city});
  }
}
