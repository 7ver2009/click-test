import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    children: [
      {path: 'weather-profile', loadChildren: 
        () => import('./layouts/pages/weather-profile/weather-profile.module').then(m => m.WeatherProfileModule)},
      {path: 'selected-cities', loadChildren: 
        () => import('./layouts/pages/selected-cities/selected-cities.module').then(m => m.SelectedCitiesModule)},
      {path: '', redirectTo: 'weather-profile', pathMatch: 'full'},
      {path: '**', redirectTo: 'weather-profile'}
    ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
