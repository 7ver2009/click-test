import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { NgxsModule } from '@ngxs/store';

import { AppRoutingModule } from './app-routing.module';
import { CityWeatherStateModule } from './store';
import { HeaderModule } from './layouts/header';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    NgxsModule.forRoot([]),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CityWeatherStateModule,
    HeaderModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
