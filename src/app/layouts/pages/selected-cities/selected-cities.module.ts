import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SelectedCitiesComponent } from './selected-cities.component';
import { SelectedCitiesRoutingModule } from './selected-cities-routing.module';

@NgModule({
  imports: [
    SelectedCitiesRoutingModule,
    CommonModule,
    FormsModule
  ],
  declarations: [
    SelectedCitiesComponent,
  ],
})

export class SelectedCitiesModule {
}
