import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Store } from '@ngxs/store';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, BehaviorSubject, combineLatest, Subject} from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';

import { CityWeatherState, weatherActions } from '../../../store';

@Component({
  selector: 'app-selected-cities',
  templateUrl: './selected-cities.component.html',
  styleUrls: ['./selected-cities.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectedCitiesComponent implements OnInit, OnDestroy {

  public selectedCitieslist$: Observable<string[]>;

  private destroy$ = new Subject();

  private readonly selectedCitiesSource = new BehaviorSubject<undefined | string[]>(undefined)
  private selectedCities$: Observable<undefined | string[]>;

  private readonly filteredSelectedSource = new BehaviorSubject<undefined | string[]>(undefined)
  private filteredSelectedCities$: Observable<undefined | string[]>;

  constructor(private readonly store: Store,
              private readonly router: Router,
              private readonly route: ActivatedRoute) { 
    this.selectedCities$ = this.selectedCitiesSource.asObservable()
      .pipe(takeUntil(this.destroy$));
    this.filteredSelectedCities$ = this.filteredSelectedSource.asObservable()
      .pipe(takeUntil(this.destroy$));
    this.selectedCitieslist$ = this.getSelectedCitiesStream();
    this.startSelectedCitiesStream();
  }

  ngOnInit() {
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public onSearchDataChange(text: string): void {
    text 
      ? this.filteredSelectedSource.next(this.getItemsListFilteredBySearch(text))
      : this.filteredSelectedSource.next(undefined)
  }

  public navigateToProfile(city: string) {
    this.store.dispatch(new weatherActions.SelectCity(city));
    this.router.navigate(['weather-profile'], {relativeTo: this.route});
  }

  private startSelectedCitiesStream(): void {
    this.store.select(CityWeatherState.getSelectedCityList)
      .pipe(
        takeUntil(this.destroy$),
        tap((cityList: string[]) => this.selectedCitiesSource.next(cityList))
      )
      .subscribe()
  }

  private getSelectedCitiesStream(): Observable<string[]> {
    return combineLatest([
      this.selectedCities$,
      this.filteredSelectedCities$
    ])
    .pipe(
      map((data: [string[], undefined | string[]]) => {
        return data[1] ? data[1] : data[0];
      })
    )
  }

  private getItemsListFilteredBySearch(text: string): string[] {
    return this.selectedCitiesSource.value
      .filter((city: string) => city.match(new RegExp(text, 'i')));
  }
}
