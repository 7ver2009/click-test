import { NgModule } from '@angular/core';
import {
    RouterModule,
    Routes
} from '@angular/router';

import { SelectedCitiesComponent } from './selected-cities.component';

const routes: Routes = [
  {
    path: '',
    component: SelectedCitiesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SelectedCitiesRoutingModule {
}
