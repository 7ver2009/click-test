import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { WeatherProfileComponent } from './weather-profile.component';
import { WeatherProfileRoutingModule } from './weather-profile-routing.module';

@NgModule({
  imports: [
    WeatherProfileRoutingModule,
    ReactiveFormsModule,
    CommonModule
  ],
  declarations: [
    WeatherProfileComponent,
  ],
})

export class WeatherProfileModule {
}
