import { Component, ChangeDetectionStrategy, OnInit, OnDestroy} from '@angular/core';
import { Store } from '@ngxs/store';
import { Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { shareReplay, catchError, first, tap} from 'rxjs/operators';

import { CityWeather } from '../../../models';
import { CityWeatherSelectors, weatherActions, CityWeatherState } from '../../../store';
import { SYNCHRONIZATION_INTERVAL } from '../../../common';

@Component({
  selector: 'app-weather-profile',
  templateUrl: './weather-profile.component.html',
  styleUrls: ['./weather-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeatherProfileComponent implements OnInit, OnDestroy {

  public cityWeatherItem$: Observable<CityWeather>;
  public control: FormControl;

  private synchronizationInterval: any;

  constructor(
    private readonly store: Store,
  ) {
    this.cityWeatherItem$ = this.getCityWeatherStream();
    this.control = new FormControl('', [Validators.required]);
   }

  public ngOnInit(): void {
    if (this.store.selectSnapshot(CityWeatherState.getCurrentCity)) {
      this.dispatchGetItemsAction();
      this.setSynchronization();
    }
  }

  public ngOnDestroy(): void {
    if (this.synchronizationInterval) {
      clearInterval(this.synchronizationInterval);
    }
  }

  public addNewCity() {
    this.store.dispatch(new weatherActions.SaveCity(this.control.value))
      .pipe(
        first(),
        tap(() => this.setSynchronization()),
        catchError(() => {
          alert('City name incorrect');
          return of(undefined);
        })
      )
      .subscribe();
  }

  public dispatchGetItemsAction(city?: string): void {
    this.store.dispatch(new weatherActions.ReceiveCityWeatherModel(city));
  }

  private getCityWeatherStream(): Observable<CityWeather> {
    return this.store.select(CityWeatherSelectors.getWeatherForCurrentCity)
      .pipe(shareReplay({bufferSize: 1, refCount: true}));
  }

  private setSynchronization(): void {
    if (this.synchronizationInterval) {
      clearInterval(this.synchronizationInterval);
    }
    this.synchronizationInterval = setInterval(() => {
        this.dispatchGetItemsAction();
      }, SYNCHRONIZATION_INTERVAL);
  }
}
