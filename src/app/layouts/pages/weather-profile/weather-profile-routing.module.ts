import { NgModule } from '@angular/core';
import {
    RouterModule,
    Routes
} from '@angular/router';

import { WeatherProfileComponent } from './weather-profile.component';

const routes: Routes = [
  {
    path: '',
    component: WeatherProfileComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WeatherProfileRoutingModule {
}
