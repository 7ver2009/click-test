export enum LocalStorageKeys {
  SELECTED_CITIES = 'selectedCities',
  CURRENT_CITY = 'currentCity'
}
